<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ContactTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testGetContacts()
    {
        $response = $this->getJson('/api/contacts');

        $response->assertStatus(200);
    }

    /**
     * Post a contact.
     *
     * @return void
     */
    public function testPostContact()
    {
        $response = $this->postJson('/api/contacts', ['name' => 'Sally', 'email' => 'a@b.com', 'phonenum' => '123']);

        $response->assertStatus(200);
    }
}
