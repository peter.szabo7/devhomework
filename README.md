## Full Stack Developer Home Assignment

Setting up the project:

- Clone repo
- Set up your local server with MySql connection (I've used Laragon)
- Set up .env file accordingly or use my .env.example file and name the database accordingly
- Run npm install & composer install
- Run php artisan migrate
- Run php artisan db:seed
- Run npm run dev
- Start up your local server & enjoy!

## Comments

The app has been optimized for Chrome browser.
I have changed some things compared to the instructions for a better user experience & code quality:

- I overwrote some bootstrap color variables with Vodafone colors
- I have changed the view, edit, add contact functions to use an animated modal window for faster user experience
- I have added some additional transition effects
- Also added some buttons to the contact cards which is easier to find for a user
- I have used a form generator to shorten the HTML part of the main component
- A simple unit test has also been created as a bonus task :)
