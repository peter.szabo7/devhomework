export default {
    data: () => {
        return {
            schema: {
                fields: [
                    {
                        type: 'input',
                        inputType: 'text',
                        label: 'Name',
                        model: 'name',
                        values: [],
                        styleClasses: "col-md-12",
                        required: true,
                        featured: true,
                        validator: ["string", "required"]
                    },
                    {
                        type: 'input',
                        inputType: 'text',
                        label: 'E-mail',
                        model: 'email',
                        values: [],
                        styleClasses: "col-md-12",
                        required: true,
                        featured: true,
                        validator: ["email", "required"]
                    },
                    {
                        type: 'input',
                        inputType: 'text',
                        label: 'Phone Number',
                        model: 'phonenum',
                        values: [],
                        styleClasses: "col-md-12",
                        required: true,
                        featured: true,
                        validator: ["string", "required"]
                    },
                ]
            },
        }
    }
}
