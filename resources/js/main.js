require('./bootstrap');

import Vue from 'vue'
import BootstrapVue from 'bootstrap-vue'
import App from './App'
import Axios from 'axios';


//for axios to include CSRF token
window.axios = Axios;
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

if(window.bw !== undefined){
    window.axios.defaults.headers.common['X-CSRF-TOKEN'] = window.bw.csrf;
    window.axios.defaults.baseUrl = window.bw.baseUrl;
} else {
    window.axios.defaults.headers.common['X-CSRF-TOKEN'] = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
    window.axios.defaults.baseUrl = window.location.origin;
}

//use bootstrap
Vue.use(BootstrapVue);

//Vue instance
new Vue({
    el: '#app',
    template: '<App/>',
    components: {
        App
    },
});

