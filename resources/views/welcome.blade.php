<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Dev Homework</title>

    <!-- Main styles for this application -->
    <link href="{{mix('css/app.css')}}" rel="stylesheet">

    <!-- Styles required by this views -->

</head>

<body class="app">
<div id="app"></div>
<script src="{{mix('js/vendor.js')}}"></script>
<script src="{{mix('js/main.js')}}"></script>

</body>

</html>
