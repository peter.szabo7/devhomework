<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use \App\Contact;

class ContactController extends Controller
{
    public function index(){

        $contacts = Contact::all();

        return $contacts;
    }

    public function destroy($id) {

        Contact::destroy($id);

        return response(null, Response::HTTP_OK);
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required|email|unique:App\Contact,email',
            'phonenum' => 'required',
        ]);

        $contact = Contact::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'phonenum' => $request['phonenum'],
        ]);

        return response()->json([
            'contact' => $contact,
            'message' => 'Contact created successfully!'
        ], 200);

    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required',
            'phonenum' => 'required',
        ]);

        $contact = Contact::where('id', '=', $id)->first();
        $contact->name = request('name');
        $contact->email = request('email');
        $contact->phonenum = request('phonenum');
        $contact->save();


        return response()->json([
            'contact' => $contact,
            'message' => 'Contact updated successfully!'
        ], 200);

    }
}
